import {createStore} from "solid-js/store";

import type {Cents, Quantity} from "./utils";

export interface Coins {
  coinValue: Cents;
  quantity: Quantity;
}

const defaultCoins = (): Array<Coins> => [
  {coinValue: 5 as Cents, quantity: 0 as Quantity},
  {coinValue: 10 as Cents, quantity: 0 as Quantity},
  {coinValue: 20 as Cents, quantity: 0 as Quantity},
  {coinValue: 50 as Cents, quantity: 0 as Quantity},
  {coinValue: 100 as Cents, quantity: 0 as Quantity},
  {coinValue: 200 as Cents, quantity: 0 as Quantity},
];

const emptyCoins = (): Coins => ({
  coinValue: 0 as Cents,
  quantity: 0 as Quantity,
});

export const createEntries = () => {
  const [entries, setEntries] = createStore<Coins[]>(defaultCoins());

  const add = () => setEntries([...entries, emptyCoins()]);

  const remove = (index: number) => {
    const removed = [
      ...entries.slice(0, index),
      ...entries.slice(index + 1, entries.length),
    ];
    if (removed.length === 0) {
      removed.push(emptyCoins());
    }
    setEntries(removed);
  };

  const edit = setEntries;

  return {
    value: entries,
    add,
    remove,
    edit,
  };
};

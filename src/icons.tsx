import {VoidComponent, JSX} from "solid-js";

/**
 * Material Icon "Close" with 700 weight, 0 grade, 20px optical size.
 */
export const Close: VoidComponent<JSX.SvgSVGAttributes<SVGSVGElement>> = (
  props
) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    height="20"
    viewBox="0 -960 960 960"
    width="20"
    {...props}
  >
    <path d="m291-208-83-83 189-189-189-189 83-83 189 189 189-189 83 83-189 189 189 189-83 83-189-189-189 189Z" />
  </svg>
);

import type {Component} from "solid-js";

import Entries from "./components/Entries";
import {createEntries} from "./entries";

const App: Component = () => {
  const entries = createEntries();

  return (
    <div class="flex flex-col items-center">
      <div class="w-96 px-2 pt-4 max-w-full">
        <h1 class="pb-4 text-3xl font-bold">Coin counter</h1>
        <Entries entries={entries} />
      </div>
    </div>
  );
};

export default App;

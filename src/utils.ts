// https://www.typescriptlang.org/play#example/nominal-typing
/**
 * Cents should be positive integers.
 */
export type Cents = number & {__brand: "cents"};

export const numberToCents = (n: number): Cents =>
  Math.max(Math.floor(n), 0) as Cents;

export const parseDollarsAsCents = (s: string): Cents | undefined => {
  const parsed = parseFloat(s);
  if (isNaN(parsed) || parsed < 0) {
    return undefined;
  }
  return numberToCents(parsed * 100);
};

export const formatCentsAsDollars = (cents: Cents) => (cents / 100).toFixed(2);

// https://www.typescriptlang.org/play#example/nominal-typing
/**
 * Quantity should be positive integers.
 */
export type Quantity = number & {__brand: "quantity"};

export const numberToQuantity = (n: number): Quantity =>
  Math.max(Math.floor(n), 0) as Quantity;

export const parseQuantity = (s: string): Quantity | undefined => {
  const parsed = parseInt(s);
  if (isNaN(parsed) || parsed < 0) {
    return undefined;
  }
  return numberToQuantity(parsed);
};

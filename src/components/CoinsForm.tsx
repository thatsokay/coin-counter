import {Component, For, onMount} from "solid-js";

import type {Coins} from "../entries";
import {
  Cents,
  Quantity,
  formatCentsAsDollars,
  numberToQuantity,
  parseDollarsAsCents,
  parseQuantity,
} from "../utils";

interface Props {
  entry: Coins;
  setCoinValue: (value: Cents) => void;
  setQuantity: (value: Quantity) => void;
}

const CoinsForm: Component<Props> = (props) => {
  let coinValueInputRef: HTMLInputElement | undefined;
  let quantityInputRef: HTMLInputElement | undefined;

  const handleEditCoinValue = () => {
    if (!coinValueInputRef) return;
    const value = coinValueInputRef.value;
    const cents = parseDollarsAsCents(value);
    if (cents === undefined) {
      props.setCoinValue(0 as Cents);
      return;
    }
    coinValueInputRef.value = formatCentsAsDollars(cents);
    props.setCoinValue(cents);
  };

  const handleEditQuantity = () => {
    if (!quantityInputRef) return;
    const value = quantityInputRef.value;
    const quantity = parseQuantity(value);
    if (quantity === undefined) {
      props.setQuantity(0 as Quantity);
      return;
    }
    quantityInputRef.value = `${quantity}`;
    props.setQuantity(quantity);
  };

  const addQuantity = (amount: number) => () => {
    if (!quantityInputRef) return;
    const newQuantity = numberToQuantity(props.entry.quantity + amount);
    quantityInputRef.value = `${newQuantity}`;
    props.setQuantity(newQuantity);
  };

  onMount(() => {
    if (props.entry.coinValue === 0) {
      coinValueInputRef?.focus();
    } else {
      if (coinValueInputRef) {
        coinValueInputRef.value = formatCentsAsDollars(props.entry.coinValue);
      }
    }
  });

  const addQuantityButtonAmounts = [-5, -1, 1, 5] as const;

  return (
    <form
      class="contents"
      onSubmit={(event) => {
        event.preventDefault();
        handleEditCoinValue();
        handleEditQuantity();
      }}
    >
      <div class="flex">
        <div class="min-w-7 flex justify-center items-center bg-neutral-100 border-2 border-r-0 border-gray-950 box-border">
          $
        </div>
        <input
          class="text-right"
          ref={coinValueInputRef}
          onFocus={() => coinValueInputRef?.select()}
          onBlur={handleEditCoinValue}
          placeholder="0.00"
          inputMode="decimal"
          tabIndex="1"
        />
      </div>
      <input
        class="text-right"
        ref={quantityInputRef}
        onFocus={() => quantityInputRef?.select()}
        onBlur={handleEditQuantity}
        placeholder="0"
        inputMode="numeric"
        tabIndex="1"
      />
      <For each={addQuantityButtonAmounts}>
        {(amount) => (
          <button
            class="p-0 text-sm"
            type="button"
            onClick={addQuantity(amount)}
            tabindex="3"
          >
            {amount > 0 && "+"}
            {amount}
          </button>
        )}
      </For>
      {/*  Hidden button to allow form submit. */}
      <button type="submit" hidden />
    </form>
  );
};

export default CoinsForm;

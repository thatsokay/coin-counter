import {Component, For} from "solid-js";

import {createEntries} from "../entries";
import {Cents, formatCentsAsDollars} from "../utils";
import {Close} from "../icons";

import CoinsForm from "./CoinsForm";

interface Props {
  entries: ReturnType<typeof createEntries>;
}

const Entries: Component<Props> = (props) => {
  const sum = () =>
    props.entries.value.reduce(
      (acc, entry) => (entry.coinValue * entry.quantity + acc) as Cents,
      0 as Cents
    );

  return (
    <div class="grid grid-cols-entries gap-x-1 gap-y-2">
      <label class="px-1">Coin value</label>
      <label class="px-1 text-right">Quantity</label>
      <div class="col-span-5"></div>
      <For each={props.entries.value}>
        {(entry, i) => (
          <>
            <CoinsForm
              entry={entry}
              setCoinValue={(value) =>
                props.entries.edit(i(), "coinValue", value)
              }
              setQuantity={(value) =>
                props.entries.edit(i(), "quantity", value)
              }
            />
            <button
              class="button-warning p-0 text-sm flex justify-center items-center"
              onClick={() => props.entries.remove(i())}
              tabIndex="3"
            >
              <Close class="fill-current" />
            </button>
          </>
        )}
      </For>
      <div class="col-span-full">
        <button onClick={() => props.entries.add()} tabindex="2">
          Add coin
        </button>
      </div>
      <div class="mt-4 col-span-full">
        <div class="p-4 bg-neutral-100 text-xl">
          <span class="font-semibold">Total:</span>
          <span class="font-normal"> ${formatCentsAsDollars(sum())}</span>
        </div>
      </div>
    </div>
  );
};

export default Entries;

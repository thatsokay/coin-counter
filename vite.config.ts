import {defineConfig} from "vite";
import solid from "vite-plugin-solid";

export default defineConfig({
  base: "./",
  publicDir: "static",
  build: {
    outDir: "public",
  },
  plugins: [solid()],
});
